﻿Imports System.ComponentModel

Public Class UserControl1
    Public Enum Depan
        One
        Two
        Three
        Four
        Five
        Six
        Seven
        Eight
        Nine
        Ten
        Jack
        Queen
        King
    End Enum

    Public Enum Jenis
        Clubs = 0
        Diamonds = 1
        Hearts = 2
        Spade = 3
    End Enum

    Private _depan As Depan
    Private _jenis As Jenis
    Private _Flip As Boolean
    Private _browsableAttribute As Boolean


    <Browsable(True)> _
    Public Property Depan_kartu As Depan
        Get
            Return _depan
        End Get
        Set(ByVal value As Depan)
            _depan = value
            cardDefinition(_jenis, _depan)
        End Set
    End Property

    Private Property BrowsableAttribute(depan As Depan) As Boolean
        Get
            Return _browsableAttribute
        End Get
        Set(value As Boolean)
            _browsableAttribute = value
        End Set
    End Property


    Sub cardDefinition(ByVal jenis As Jenis, ByVal depan As Depan)
        Select Case jenis
            Case jenis.Clubs
                If _depan = depan.One Then
                    PictureBox1.Image = My.Resources.C1
                ElseIf depan = depan.Two Then
                    PictureBox1.Image = My.Resources.C2
                ElseIf depan = depan.Three Then
                    PictureBox1.Image = My.Resources.C3
                ElseIf depan = depan.Four Then
                    PictureBox1.Image = My.Resources.C4
                ElseIf depan = depan.Five Then
                    PictureBox1.Image = My.Resources.C5
                ElseIf depan = depan.Six Then
                    PictureBox1.Image = My.Resources.C6
                ElseIf depan = depan.Seven Then
                    PictureBox1.Image = My.Resources.C7
                ElseIf depan = depan.Eight Then
                    PictureBox1.Image = My.Resources.C8
                ElseIf depan = depan.Nine Then
                    PictureBox1.Image = My.Resources.C9
                ElseIf depan = depan.Ten Then
                    PictureBox1.Image = My.Resources.C10
                ElseIf depan = depan.Jack Then
                    PictureBox1.Image = My.Resources.CJ
                ElseIf depan = depan.Queen Then
                    PictureBox1.Image = My.Resources.CQ
                ElseIf depan = depan.King Then
                    PictureBox1.Image = My.Resources.CK
                End If
            Case jenis.Diamonds
                If _depan = depan.One Then
                    PictureBox1.Image = My.Resources.D1
                ElseIf depan = depan.Two Then
                    PictureBox1.Image = My.Resources.D2
                ElseIf depan = depan.Three Then
                    PictureBox1.Image = My.Resources.D3
                ElseIf depan = depan.Four Then
                    PictureBox1.Image = My.Resources.D4
                ElseIf depan = depan.Five Then
                    PictureBox1.Image = My.Resources.D5
                ElseIf depan = depan.Six Then
                    PictureBox1.Image = My.Resources.D6
                ElseIf depan = depan.Seven Then
                    PictureBox1.Image = My.Resources.D7
                ElseIf depan = depan.Eight Then
                    PictureBox1.Image = My.Resources.D8
                ElseIf depan = depan.Nine Then
                    PictureBox1.Image = My.Resources.D9
                ElseIf depan = depan.Ten Then
                    PictureBox1.Image = My.Resources.D10
                ElseIf depan = depan.Jack Then
                    PictureBox1.Image = My.Resources.DJ
                ElseIf depan = depan.Queen Then
                    PictureBox1.Image = My.Resources.DQ
                ElseIf depan = depan.King Then
                    PictureBox1.Image = My.Resources.DK
                End If
            Case jenis.Hearts
                If _depan = depan.One Then
                    PictureBox1.Image = My.Resources.H1
                ElseIf depan = depan.Two Then
                    PictureBox1.Image = My.Resources.H2
                ElseIf depan = depan.Three Then
                    PictureBox1.Image = My.Resources.H3
                ElseIf depan = depan.Four Then
                    PictureBox1.Image = My.Resources.H4
                ElseIf depan = depan.Five Then
                    PictureBox1.Image = My.Resources.H5
                ElseIf depan = depan.Six Then
                    PictureBox1.Image = My.Resources.H6
                ElseIf depan = depan.Seven Then
                    PictureBox1.Image = My.Resources.H7
                ElseIf depan = depan.Eight Then
                    PictureBox1.Image = My.Resources.H8
                ElseIf depan = depan.Nine Then
                    PictureBox1.Image = My.Resources.H9
                ElseIf depan = depan.Ten Then
                    PictureBox1.Image = My.Resources.H10
                ElseIf depan = depan.Jack Then
                    PictureBox1.Image = My.Resources.HJ
                ElseIf depan = depan.Queen Then
                    PictureBox1.Image = My.Resources.HQ
                ElseIf depan = depan.King Then
                    PictureBox1.Image = My.Resources.HK
                End If
            Case jenis.Spade
                If _depan = depan.One Then
                    PictureBox1.Image = My.Resources.S1
                ElseIf depan = depan.Two Then
                    PictureBox1.Image = My.Resources.S2
                ElseIf depan = depan.Three Then
                    PictureBox1.Image = My.Resources.S3
                ElseIf depan = depan.Four Then
                    PictureBox1.Image = My.Resources.S4
                ElseIf depan = depan.Five Then
                    PictureBox1.Image = My.Resources.S5
                ElseIf depan = depan.Six Then
                    PictureBox1.Image = My.Resources.S6
                ElseIf depan = depan.Seven Then
                    PictureBox1.Image = My.Resources.S7
                ElseIf depan = depan.Eight Then
                    PictureBox1.Image = My.Resources.S8
                ElseIf depan = depan.Nine Then
                    PictureBox1.Image = My.Resources.S9
                ElseIf depan = depan.Ten Then
                    PictureBox1.Image = My.Resources.S10
                ElseIf depan = depan.Jack Then
                    PictureBox1.Image = My.Resources.SJ
                ElseIf depan = depan.Queen Then
                    PictureBox1.Image = My.Resources.SQ
                ElseIf depan = depan.King Then
                    PictureBox1.Image = My.Resources.SK
                End If
        End Select
    End Sub


    <Browsable(True), DefaultValue(Jenis.Spade)> _
    Public Property Jenis_Kartu As Jenis
        Get
            Return _jenis
            ''cardDefinition(_jenis, _depan)
        End Get

        Set(ByVal value As Jenis)
            _jenis = value
            cardDefinition(_jenis, _depan)
        End Set
    End Property


    Public Property openKartu As Boolean
        Get
            Return _Flip
            cardDefinition(_jenis, _depan)
        End Get

        Set(ByVal value As Boolean)
            _Flip = value
            If _Flip = False Then
                PictureBox1.Image = My.Resources.CL
                BrowsableAttribute(Depan_kartu) = False
                BrowsableAttribute(Jenis_Kartu) = False
            ElseIf _Flip = True Then
                PictureBox1.Image = My.Resources.C1
                BrowsableAttribute(Depan_kartu) = True
                BrowsableAttribute(Jenis_Kartu) = True
            End If

        End Set
    End Property

End Class




