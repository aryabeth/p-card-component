﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControl1
    Inherits System.Windows.Forms.UserControl

    'UserControl1 overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UserControl1))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "C1.png")
        Me.ImageList1.Images.SetKeyName(1, "C2.png")
        Me.ImageList1.Images.SetKeyName(2, "C3.png")
        Me.ImageList1.Images.SetKeyName(3, "C4.png")
        Me.ImageList1.Images.SetKeyName(4, "C5.png")
        Me.ImageList1.Images.SetKeyName(5, "C6.png")
        Me.ImageList1.Images.SetKeyName(6, "C7.png")
        Me.ImageList1.Images.SetKeyName(7, "C8.png")
        Me.ImageList1.Images.SetKeyName(8, "C9.png")
        Me.ImageList1.Images.SetKeyName(9, "C10.png")
        Me.ImageList1.Images.SetKeyName(10, "CJ.png")
        Me.ImageList1.Images.SetKeyName(11, "CK.png")
        Me.ImageList1.Images.SetKeyName(12, "CQ.png")
        Me.ImageList1.Images.SetKeyName(13, "D1.png")
        Me.ImageList1.Images.SetKeyName(14, "D2.png")
        Me.ImageList1.Images.SetKeyName(15, "D3.png")
        Me.ImageList1.Images.SetKeyName(16, "D4.png")
        Me.ImageList1.Images.SetKeyName(17, "D5.png")
        Me.ImageList1.Images.SetKeyName(18, "D6.png")
        Me.ImageList1.Images.SetKeyName(19, "D7.png")
        Me.ImageList1.Images.SetKeyName(20, "D8.png")
        Me.ImageList1.Images.SetKeyName(21, "D9.png")
        Me.ImageList1.Images.SetKeyName(22, "D10.png")
        Me.ImageList1.Images.SetKeyName(23, "DJ.png")
        Me.ImageList1.Images.SetKeyName(24, "DK.png")
        Me.ImageList1.Images.SetKeyName(25, "DQ.png")
        Me.ImageList1.Images.SetKeyName(26, "H1.png")
        Me.ImageList1.Images.SetKeyName(27, "H2.png")
        Me.ImageList1.Images.SetKeyName(28, "H3.png")
        Me.ImageList1.Images.SetKeyName(29, "H4.png")
        Me.ImageList1.Images.SetKeyName(30, "H5.png")
        Me.ImageList1.Images.SetKeyName(31, "H6.png")
        Me.ImageList1.Images.SetKeyName(32, "H7.png")
        Me.ImageList1.Images.SetKeyName(33, "H8.png")
        Me.ImageList1.Images.SetKeyName(34, "H9.png")
        Me.ImageList1.Images.SetKeyName(35, "H10.png")
        Me.ImageList1.Images.SetKeyName(36, "HJ.png")
        Me.ImageList1.Images.SetKeyName(37, "HK.png")
        Me.ImageList1.Images.SetKeyName(38, "HQ.png")
        Me.ImageList1.Images.SetKeyName(39, "S1.png")
        Me.ImageList1.Images.SetKeyName(40, "S2.png")
        Me.ImageList1.Images.SetKeyName(41, "S3.png")
        Me.ImageList1.Images.SetKeyName(42, "S4.png")
        Me.ImageList1.Images.SetKeyName(43, "S5.png")
        Me.ImageList1.Images.SetKeyName(44, "S6.png")
        Me.ImageList1.Images.SetKeyName(45, "S7.png")
        Me.ImageList1.Images.SetKeyName(46, "S8.png")
        Me.ImageList1.Images.SetKeyName(47, "S9.png")
        Me.ImageList1.Images.SetKeyName(48, "S10.png")
        Me.ImageList1.Images.SetKeyName(49, "SJ.png")
        Me.ImageList1.Images.SetKeyName(50, "SK.png")
        Me.ImageList1.Images.SetKeyName(51, "SQ.png")
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.CardComponent.My.Resources.Resources.CL
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(73, 97)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'UserControl1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "UserControl1"
        Me.Size = New System.Drawing.Size(73, 97)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Protected WithEvents PictureBox1 As System.Windows.Forms.PictureBox

End Class
